<?php
	require "conexion.php";
	header('Content-Type: application/JSON'); 
	$method = $_SERVER['REQUEST_METHOD'];

	switch ($method) {
		case 'GET':

			// getallheaders();
			$autorization = '';
			$headers = array();
			foreach (getallheaders() as $name => $value) {
				$headers[$name] = $value;
			}

			// Ternarios   variable = (comparacion) ? cumple : no cumple;
			$autorization = (isset($headers['Authorization'])) ? $headers['Authorization'] : null;

			$token = str_replace("Bearer ","",$autorization);

			if($token == "cursosMFer9897"){

				if(isset($_GET['param'])){

					$id   = intval($_GET['param']);

					$query = "SELECT * FROM usuarios where id='$id' ";
					$result   = mysqli_query($db, $query);
					
					
					if($result->num_rows > 0 ){

						while ($row = mysqli_fetch_array($result)) {
							   $user = $row['user'];
						}

						$datos = "El usuario es: ".$user;

						$response = array('response' => 'Success' ,
								  'message' =>  $datos
				 		);

				 		header("HTTP/1.1 200 OK");
						echo json_encode($response);

					}else{
						header("HTTP/1.1 404 NOT FOUND");
						echo json_encode("");
					}
						
				}else{

					$response = array('response' => 'Faild' ,
								  'message' => "No enviaste la variable param en la url"
				 	);

					header("HTTP/1.1 400 BAD REQUEST");
					echo json_encode($response);
				}

			}else{
				header("HTTP/1.1 401 AUTORIZATHED");
				echo json_encode("");
			}
			
			break;

		case 'POST':

			$autorization = '';
			$headers = array();
			foreach (getallheaders() as $name => $value) {
				$headers[$name] = $value;
			}

			$autorization = (isset($headers['Authorization'])) ? $headers['Authorization'] : null;

			$token = str_replace("Basic ","",$autorization);


			if($token == "YWRtaW46cHJ1ZWJh"){

				if(isset($_POST['usuario']) && isset($_POST['password']) && isset($_POST['email'])){

					$usuario   = $_POST['usuario'];
					$password  = $_POST['password'];
				    $email     = $_POST['email'];
					
					$sql = "INSERT usuarios (user,password,email) VALUES ('".$usuario."','".$password."','".$email."')";                                  
					
					if(mysqli_query($db, $sql)){
					
					$respuesta  = "Se a guardado el usuario: ".$usuario." Satisfactoriamente";
				
					$response = array('response' => 'Success' ,
									  'message '  =>  $respuesta
					);

					  header("HTTP/1.1 201 CREATE");
					  echo json_encode( $response );
					}else{
					   
					    $response = array('response' => 'Faild' ,
								    	  'message'  =>  'No se pudo guardar el usuario'
					    );

					  header("HTTP/1.1 400 BAD REQUEST");
					  echo json_encode( $response );
					}		

				}else{

					$postBody = file_get_contents("php://input");// sirver para obtener o extraer el contenido
					$data     = json_decode($postBody); 

                    $sql = "INSERT usuarios (user,password,email) VALUES ('".$data->usuario."','".$data->password."','".$data->email."')";    
					if(mysqli_query($db, $sql)){
					
						$respuesta  = "Se guardo el usuario: ".$data->usuario;
					
						$response = array('response' => 'Success' ,
										  'message '  =>  $respuesta
						);
	
						  header("HTTP/1.1 201 CREATE");
						  echo json_encode( $response );
						}else{
					// $data = {"nombre": "hoos"}

					$response = array('response' => 'Faild' ,
					'message'  =>  'No se pudo guardar el usuario'
                    );

                    header("HTTP/1.1 400 BAD REQUEST");
                    echo json_encode( $response );}							
				}
			}else{
				header("HTTP/1.1 401 AUTORIZATHED");
				echo json_encode( "" );
			}
			break;
		
		case 'PATCH':
            $autorization = '';
			$headers = array();
			foreach (getallheaders() as $name => $value) {
				            $headers[$name] = $value;
			}
          
			$autorization = (isset($headers['Authorization'])) ? $headers['Authorization'] : null;
            $token = str_replace("Basic ","",$autorization);

			if($token == "YWRtaW46cHJ1ZWJh"){


		 	   $parchBody = file_get_contents("php://input");
			   $data      = json_decode($parchBody);

			   $sql = "UPDATE usuarios SET user ='".$data->fields->user."',email ='".$data->fields->email."' WHERE id =$data->id";                       
               if (mysqli_query($db,$sql)){
            
				$response  = array( 'response' => 'Success' , 
								    'message'  => 'Se agrego satisfactoriamente ' 
			    );
              
				header("HTTP/1.1 200 OK");
     			echo json_encode( $response );              
			  }else{
				
				$response  = array( 'response' => 'Success' , 
								'message' =>  'No se ha podido actualizar'
			    );
							
				header("HTTP/1.1 400 BAD REQUEST");
				echo json_encode( $response );

			
			    }
			}else{
				header("HTTP/1.1 401 AUTORIZATHED");
				echo json_encode( "" );


			}
			
			break;

		case 'PUT':

			$putBody = file_get_contents("php://input");
			$data    = json_decode($putBody);
            
			$sql = "UPDATE usuarios SET user ='".$data->fields->user."' WHERE id =$data->id";                       
            if (mysqli_query($db,$sql)){
				
				 $response  = array( 'response' => 'Success' , 
				 'message'  => 'Actualizo correctamente el usuario' 
                 );

                header("HTTP/1.1 200 OK");
                echo json_encode( $response );              
            }else{

                $response  = array( 'response' => 'Success' , 
			    'message' =>  'No se ha podido actualizar'
                );
		
                header("HTTP/1.1 400 BAD REQUEST");
                echo json_encode( $response );
			}


        
	
            
			break;

		case 'DELETE':

			$autorization = '';
			$headers = array();
			foreach (getallheaders() as $name => $value) {
								$headers[$name] = $value;
			}
	
			$autorization = (isset($headers['Authorization'])) ? $headers['Authorization'] : null;
			$token = str_replace("Bearer ","",$autorization);
	
			if($token == "cursosMFer9897"){

			  $deleteBody = file_get_contents("php://input");
			  $data       = json_decode($deleteBody);
			  $sql = "DELETE FROM usuarios WHERE id=$data->id";
            
			 if (mysqli_query($db, $sql)) {

				$response  = array( 
					'response' => 'Success', 
					'message' =>  'Se ha borrado correctamente el usuario'
				);

				header("HTTP/1.1 200 OK");
				echo json_encode( $response );
			 }else{

				$response  = array( 
					'response' => 'Faild', 
					'message' =>  'No se pudo eliminar el usuario'
				);

				header("HTTP/1.1 400 OK");
				echo json_encode( $response );
			 }

			}else{
				header("HTTP/1.1 401 AUTORIZATHED");
				echo json_encode("");
		    }
			break; 

		case 'OPTIONS':
			header("HTTP/1.1 204 NO CONTENT");
			header("Access-Control-Allow-Methods: GET,PUT,PATCH,DELETE");
			break;
		
		default:
			header("HTTP/1.1 405 AUTORIZATHED");
			break;
      }

?>